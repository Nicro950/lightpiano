console.log("Hello World");

interface Rect {
    x: number;
    y: number;
    width: number;
    height: number;
}
const names = [
    "040 C4",
    // "041 C#4",
    "042 D4",
    // "043 D#4",
    "044 E4",
    "045 F4",
    // "046 F#4",
    "047 G4",
    // "048 G#4",
    "049 A4",
    // "050 A#4",
    "051 B4",
    // "052 C5",
];
const notes = names.length;
const activeNotes: boolean[] = (<boolean[]>new Array(notes)).fill(false, 0, notes);
console.log(activeNotes);

const updateCycleInterval = 50; // ms

const highLimit = 200;

function updateVideo() {}

function setupVideo(stream: MediaStream) {
    const video = <HTMLVideoElement>document.getElementById("mainvideo");
    const canvas = <HTMLCanvasElement>document.getElementById("canvas");
    const highval = <HTMLHeadingElement>document.getElementById("highval");
    const isactive = <HTMLHeadingElement>document.getElementById("isactive");

    const context = canvas.getContext("2d");

    const videoStreams = stream.getVideoTracks();

    video.srcObject = stream;

    setInterval(() => {
        if (!context) {
            throw Error("Did not get context");
        }
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;

        context.drawImage(video, 0, 0, canvas.width, canvas.height);

        const rect: Rect = { x: 0, y: 400, width: canvas.width, height: 10 };

        const img = context.getImageData(rect.x, rect.y, rect.width, rect.height);
        // let highXY: {x: number, y: number} = {x: 0, y: 0};
        // let highRGB: {r: number, g: number, b: number } = {r: 0, g: 0, b: 0};

        const scanLength = rect.width * 4;
        let pixelSectors: number[] = [];
        for (let y = 0; y < rect.height; y += 1) {
            for (let x = 0; x < scanLength; x += 4) {
                const index = x + y * scanLength;

                const r = img.data[index];
                const g = img.data[index + 1];
                const b = img.data[index + 2];

                const pixelValue = r;
                const newIndex = Math.floor((x / scanLength) * notes);

                if (!pixelSectors[newIndex] || pixelValue > pixelSectors[newIndex]) {
                    // highXY = {x, y};
                    // highRGB = {r, g, b};
                    pixelSectors[newIndex] = pixelValue;
                }
            }
        }
        highval.innerHTML = "Highest pixel val: " + pixelSectors[0];
        isactive.innerHTML = pixelSectors[0] > highLimit ? "Active!" : "Not active";
        // console.log(highNums);
        const chunkLength = rect.width / notes;
        for (let i = 0; i < pixelSectors.length; i++) {
            if (pixelSectors[i] > highLimit) {
                context.strokeStyle = "green";
                context.fillStyle = "green";
                context.fillRect(rect.x + i * chunkLength, rect.y, chunkLength, rect.height);

                if (!activeNotes[i]) {
                    playAudio(notes - i - 1);
                }

                activeNotes[i] = true;
            } else {
                context.strokeStyle = "black";
                context.fillStyle = "black";
                context.strokeRect(rect.x + i * chunkLength, rect.y, chunkLength, rect.height);

                activeNotes[i] = false;
            }
        }
        //context.strokeRect(rect.x, rect.y, rect.width,rect.height);
    }, updateCycleInterval);
}

async function Init() {
    const result = await navigator.mediaDevices.getUserMedia({ video: true });
    setupVideo(result);
}

function padNumber(num: number, size: number) {
    let str = num.toString();
    while (str.length < size) str = "0" + str;
    return str;
}

function playAudio(number: number = 0) {
    const test = new Audio(`sounds/${names[number]}.wav`);
    test.play();
}

Init();
