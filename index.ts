import { app, BrowserWindow } from "electron";

let mainWindow: BrowserWindow | undefined;

function createWindow() {
    mainWindow = new BrowserWindow({ width: 800, height: 600 });

    mainWindow.loadFile("index.html");

    mainWindow.on("closed", (event: Electron.Event, other: boolean) => {
        if (mainWindow) {
            mainWindow = undefined;
        }
    });
}

app.on("ready", createWindow);

app.on("window-all-closed", function() {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("activate", function() {
    if (!mainWindow) {
        createWindow();
    }
});
